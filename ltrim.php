<?php

/* =================ltrim — Strip whitespace (or other characters) from the beginning of a string================== */
##inpute type:
/* =================string ltrim ( string $str [, string $character_mask ] )======================= */

$text = "\t\tThese are a few words :) ...  ";
$binary = "\x09Example string\x0A";
$hello = "Hello World";
var_dump($text, $binary, $hello);
echo '</br>';

print "\n";


$trimmed = ltrim($text);
var_dump($trimmed);
echo '</br>';

$trimmed = ltrim($text, " \t.");
var_dump($trimmed);
echo '</br>';

$trimmed = ltrim($hello, "Hdle");
var_dump($trimmed);
echo '</br>';

// trim the ASCII control characters at the beginning of $binary
// (from 0 to 31 inclusive)
$clean = ltrim($binary, "\x00..\x1F");
var_dump($clean);
