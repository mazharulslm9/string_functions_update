<?php

/* =================str_replace — Replace all occurrences of the search string with the replacement string================== */
##inpute type:
/* =================mixed str_replace ( mixed $search , mixed $replace , mixed $subject [, int &$count ] )======================= */

// Provides: <body text='red'>
echo $bodytag = str_replace("%body%", "red", "<body text='%body%'>");
echo '</br>';

// Provides: Hll Dvlprs f PHP
$vowels = array("a", "e", "i", "o", "u", "A", "E", "I", "O", "U");
echo $onlyconsonants = str_replace($vowels, "", "Hello Developers of PHP");
echo '</br>';

// Provides: You should eat pizza, beer, and ice cream every day
$phrase = "You should eat fruits, vegetables, and fiber every day.";
$healthy = array("fruits", "vegetables", "fiber");
$yummy = array("pizza", "beer", "ice cream");

echo $newphrase = str_replace($healthy, $yummy, $phrase);
echo '</br>';

// Provides: 5
$str = str_replace($vowels, "", "good golly miss molly!", $count);
echo $count;
