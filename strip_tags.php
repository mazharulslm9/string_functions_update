<?php

/* =================strip_tags — Strip HTML and PHP tags from a string================== */
##inpute type:
/* =================string strip_tags ( string $str [, string $allowable_tags ] )======================= */
$text = '<p>Test paragraph.</p><!-- Comment --> <a href="#fragment">Another text</a>';
echo strip_tags($text);
echo "\n";

// Allow <p> and <a>
echo strip_tags($text, '<p><a>');
