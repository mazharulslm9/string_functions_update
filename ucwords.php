<?php

/* =================ucwords — Uppercase the first character of each word in a string================== */
##inpute type:
/* =================string ucwords ( string $str [, string $delimiters = " \t\r\n\f\v" ] )======================= */

$foo = 'hello bitm!';
echo $foo = ucwords($foo);             // Hello World!
echo '</br>';
$bar = 'HELLO BITM!';
echo $bar = ucwords($bar);             // HELLO WORLD!
echo '</br>';
echo $bar = ucwords(strtolower($bar)); // Hello World!

