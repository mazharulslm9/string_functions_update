<?php

/* =================str_pad — Pad a string to a certain length with another string================== */
##inpute type:
/* =================string str_pad ( string $input , int $pad_length [, string $pad_string = " " [, int $pad_type = STR_PAD_RIGHT ]] )======================= */

$input = "Mazhar";
echo str_pad($input, 10);                      // produces "Alien     "
echo '</br>';
echo str_pad($input, 10, "*=", STR_PAD_LEFT);  // produces "-=-=-Alien"
echo '</br>';
echo str_pad($input, 10, "_", STR_PAD_BOTH);   // produces "__Alien___"
echo '</br>';
echo str_pad($input, 6, "___");               // produces "Alien_"
echo '</br>';
echo str_pad($input, 3, "`");                 // produces "Alien"