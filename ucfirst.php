<?php

/* =================ucfirst — Make a string's first character uppercase================== */
##inpute type:
/* =================string ucfirst ( string $str )======================= */
$foo = 'hello PHP!';
echo $foo = ucfirst($foo);             // Hello PHP!
echo '</br>';
$bar = 'HELLO PHP!';
echo $bar = ucfirst($bar);             // HELLO PHP!
echo '</br>';
echo $bar = ucfirst(strtolower($bar)); // Hello php!