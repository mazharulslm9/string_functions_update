<?php

/* =================substr_compare — Binary safe comparison of two strings from an offset, up to length characters================== */
##inpute type:
/* =================int substr_compare ( string $main_str , string $str , int $offset [, int $length [, bool $case_insensitivity = false ]] )======================= */

echo substr_compare("abcde", "bc", 1, 2); // 0
echo '</br>';
echo substr_compare("abcde", "de", -2, 2); // 0
echo '</br>';
echo substr_compare("abcde", "bcg", 1, 2); // 0
echo '</br>';
echo substr_compare("abcde", "BC", 1, 2, true); // 0
echo '</br>';
echo substr_compare("abcde", "bc", 1, 3); // 1
echo '</br>';
echo substr_compare("abcde", "cd", 1, 2); // -1
